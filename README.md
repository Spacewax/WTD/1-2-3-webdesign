# 1-2-3 Web Design
<!---
[![Software License](https://img.shields.io/badge/License-CreativeCommons-orange.svg)](http://creativecommons.org/licenses/by/4.0/)
--->

is based on this [tutorial](https://codingthesmartway.com/build-a-real-world-html5-css3-responsive-website-from-scratch/).


## Exerpt from that site


>A real-world website with pure HTML 5 and CSS 3 which can be used as a template for a web design agency or any other business website.
> Let’s take a look at the final result first:
>
>The website template is fully responsible and consists of three pages. The start page looks like the following:

![alt text](https://codingthesmartway.com/wp-content/uploads/2017/06/01-1.png "Original Tutorial Imaage")